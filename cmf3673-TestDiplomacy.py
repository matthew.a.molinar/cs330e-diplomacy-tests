#!/usr/bin/env python3


# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import Battlefield
from Diplomacy import diplomacy_solve
# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")


# ----
# main
# ----
if __name__ == "__main__":
    main()
